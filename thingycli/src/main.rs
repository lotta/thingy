//! # thingycli

mod cmds;

use std::ops::Deref;

use clap::{Parser, Subcommand};
use cmds::*;
use libthingy::Thingy as LibThingyThingy;
use libthingy::CRUD;

// TODO: this should impl deref
#[derive(Debug, Clone)]
pub struct Thingy {
    wrapped: LibThingyThingy
}

impl Deref for Thingy {
    type Target = LibThingyThingy;

    fn deref(&self) -> &Self::Target {
        &self.wrapped
    }
}

// i want to derive the parser for the libthingy in this scope only

#[derive(Parser)]
#[command(version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

// a thing that identifies a Thingy like its id or some description
#[derive(Debug, Clone)]
enum ThingyIdent {
    Id(u64),
    Text(String), // TODO: make this a Vec of Strings or just of chars in general so that we get
                  // the whitespace with this
}

impl std::str::FromStr for ThingyIdent {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let id_arg = s.parse::<u64>().ok();

        match id_arg {
            Some(id_arg) => Ok(ThingyIdent::Id(id_arg)),
            None => { 
                // we try looking it up if there is just at least one character
                match s.len() {
                    0 => Err(Error::ThingyArgEmptyString),
                    _ => Ok(ThingyIdent::Text(s.to_string()))
                }
            },
        }
    }
}

#[derive(Subcommand)]
enum Commands {
    Add { 
        text: Vec<String>,

        /// add to a specific project
        #[arg(short = 'p', value_name = "PROJECT")]
        project: Option<String>, 
    },
    Rm  {
        thingy_ident: ThingyIdent,
    },
    Ls {
        opt_thingy_ident: Option<ThingyIdent>
    }
}

#[derive(Debug)]
enum Error {
    NotSetup,
    ThingyArgEmptyString,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
           Error::NotSetup => "setup has not been ran, please run `thingy setup`",
           Error::ThingyArgEmptyString => "a thingy argument was required but not provided",
        };
        write!(f, "{msg}")
    }
}

impl std::error::Error for Error {
}

enum ParseErr {
    Not
}

fn handle_invocation() -> Result<(), Error> {
    let args = Cli::parse();

    let x = match args.command {
        Commands::Add { 
            text,
            project
        } => {
            let text = text.join(" ");
            match project {
                None => println!("adding: <{}>", text),
                Some(p) => println!("adding: <{}> to project <{}>", text, p),
            }

            let thingy = LibThingyThingy::new(text);
            let res = thingy.save();
        }
        Commands::Ls { 
            opt_thingy_ident,
        }=> {
            println!("ls {:?}", opt_thingy_ident)
        },
        Commands::Rm { thingy_ident } => {
            println!("rm")
        },
    };

    Ok(())
}

fn assert_setup() -> Result<(), Error> {
    Ok(())
    // Err(Error::NotSetup)
}

fn main() -> Result<(), Error> {
    let res = assert_setup();
    if res.is_err() {
        return res
    }

    handle_invocation()
}
