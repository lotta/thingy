# https://vtimofeenko.com/posts/practical-nix-flake-anatomy-a-guided-tour-of-flake.nix/#nixosmodules
# https://xeiaso.net/blog/nix-flakes-1-2022-02-21/
# https://nixos.wiki/wiki/Overlays
{
  description = "thingy";

  inputs.nixpkgs.url = "nixpkgs/nixos-23.11";
  inputs.rust-overlay.url = "github:oxalica/rust-overlay";
  # TODO: support macOS
  # inputs.nix-darwin.url = "github:LnL7/nix-darwin";
  # inputs.nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  # also then cross compile in the guy

  outputs = { self, nixpkgs, rust-overlay, ... }:
    let
      rustVersion = "1.78.0";
      version = builtins.substring 0 8 self.lastModifiedDate;
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      nixpkgsFor = forAllSystems (system:
        import nixpkgs {
          inherit system;
          overlays = [
            (import rust-overlay)
          ];
        });

      buildRustPackageForRustBin = pkgs: 
          let
              rustPlatform = pkgs.makeRustPlatform {
                cargo = pkgs.rust-bin.stable.${rustVersion}.minimal;
                rustc = pkgs.rust-bin.stable.${rustVersion}.minimal;
              };
          in
          rustPlatform.buildRustPackage;

    in
    rec {
      # TODO: maybe also just have a nixos module here?

      overlays = {
          thingycliOverlay = final: prev: {
            # TODO: support sqlx macros -> set DATABASE_URL to compile against
            # run `cargo sqlx prepare`  -> but that also wants a DATABASE URL iirc
            thingycli = buildRustPackageForRustBin final {
              pname = "thingycli";
              inherit version;
              
              cargoLock = { lockFile = ./Cargo.lock; };
              src = ./.;
            };
          };
      };

      nixosConfigurations = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          "thingy_dev" = nixpkgs.lib.nixosSystem {
            inherit system;
            pkgs = pkgs.extend overlays.thingycliOverlay;

            modules = [
              ./dev_env/configuration.nix
            ];
          };
        });
    };
}
