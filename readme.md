# thingy - a task management backend with cli

## goals

- [ ] have a server that you can interact with a RPC (msgpack-rpc )
  - and when that is running the guy through there (feature flag)
  - but this basically would then implement the LSP protocol (feature flag)
  - https://github.com/msgpack-rpc/msgpack-rpc/tree/master
  - (either rpc or the thing with the guy)

- [ ] have a cli that can just like interact with the sqlite file (baseline)
  - it should have fuzzy find like autocomletion (like you can start typing the ids and it will just get them and show you the first 15chars for each)
  - and also the stuff for like projects that needs to come dynamically from the guy
- [ ] be able to sync (using webdav) (feature flag)
- [ ] get a calendar representation (feature flag)

### do i really want a server that is always running or do i just want an sqlite file?

- sqlite is nice because its not that resource intensive 
- although the server would probably also be fine
- then it is a thing of having a cron job that just periodically syncs
  - or just on every update

- the serve guy seems kinda nice so that you can just keep things cached and really fast. 
  - allthough querying an sqlite database that only has like a coulple thousand rows is also fast

## dev stuff 

### building the test vm

` nix build .#nixosConfigurations.x86_64-linux.thingy_dev.config.system.build.vm`

`./result/activate`
