#[derive(Debug, Clone)]
pub struct Thingy {
    text: String
}

impl Thingy { 
    pub fn new(text: String) -> Self { 
        Thingy {
            text
        }
    }

    // pub fn save(&self) {
    //     sqlx::query!("").
    // }
}

// we'd need macros here to get this done nicely with different arguments for a trait
pub trait CRUD {
    // fn create() -> Result<(), ()>;
    fn update(&self) -> Result<(), ()>;
    // fn fetch() -> Result<(), ()>;
    fn destroy(&self) -> Result<(), ()>;
    fn save(&self) -> Result<(), ()>;
}

impl CRUD for Thingy {
    fn destroy(&self) -> Result<(), ()> {
        todo!()
    }

    fn update(&self) -> Result<(), ()> {
        todo!()
    }

    fn save(&self) -> Result<(), ()> {
        // sqlx::query!("SELECT");
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
    }
}
